PGDMP  +    2    
            |            batch7     15.6 (Ubuntu 15.6-1.pgdg22.04+1)     16.2 (Ubuntu 16.2-1.pgdg22.04+1)     D           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            E           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            F           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            G           1262    92476    batch7    DATABASE     r   CREATE DATABASE batch7 WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'en_US.UTF-8';
    DROP DATABASE batch7;
                postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
                pg_database_owner    false            H           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                   pg_database_owner    false    4            �            1259    92482    barang_id_seq    SEQUENCE     v   CREATE SEQUENCE public.barang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.barang_id_seq;
       public          postgres    false    4            �            1259    92763    merchant    TABLE     �   CREATE TABLE public.merchant (
    id uuid NOT NULL,
    merchant_name character varying(255),
    merchant_location character varying(255),
    open boolean
);
    DROP TABLE public.merchant;
       public         heap    postgres    false    4            �            1259    92790    order_detail    TABLE     y   CREATE TABLE public.order_detail (
    id uuid NOT NULL,
    order_id uuid,
    product_id uuid,
    quantity integer
);
     DROP TABLE public.order_detail;
       public         heap    postgres    false    4            �            1259    92770    orders    TABLE     �   CREATE TABLE public.orders (
    id uuid NOT NULL,
    order_time date,
    destination_address character varying(255),
    user_id uuid,
    completed boolean
);
    DROP TABLE public.orders;
       public         heap    postgres    false    4            �            1259    92780    product    TABLE     �   CREATE TABLE public.product (
    id uuid NOT NULL,
    product_name character varying(255),
    price double precision,
    merchant_id uuid
);
    DROP TABLE public.product;
       public         heap    postgres    false    4            �            1259    92756    users    TABLE     �   CREATE TABLE public.users (
    id uuid NOT NULL,
    username character varying(255),
    email_address character varying(255),
    password character varying(255)
);
    DROP TABLE public.users;
       public         heap    postgres    false    4            >          0    92763    merchant 
   TABLE DATA           N   COPY public.merchant (id, merchant_name, merchant_location, open) FROM stdin;
    public          postgres    false    216   ^       A          0    92790    order_detail 
   TABLE DATA           J   COPY public.order_detail (id, order_id, product_id, quantity) FROM stdin;
    public          postgres    false    219   %       ?          0    92770    orders 
   TABLE DATA           Y   COPY public.orders (id, order_time, destination_address, user_id, completed) FROM stdin;
    public          postgres    false    217   �       @          0    92780    product 
   TABLE DATA           G   COPY public.product (id, product_name, price, merchant_id) FROM stdin;
    public          postgres    false    218   �        =          0    92756    users 
   TABLE DATA           F   COPY public.users (id, username, email_address, password) FROM stdin;
    public          postgres    false    215   �!       I           0    0    barang_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.barang_id_seq', 1, false);
          public          postgres    false    214            �           2606    92769    merchant merchant_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.merchant
    ADD CONSTRAINT merchant_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.merchant DROP CONSTRAINT merchant_pkey;
       public            postgres    false    216            �           2606    92794    order_detail order_detail_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.order_detail
    ADD CONSTRAINT order_detail_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.order_detail DROP CONSTRAINT order_detail_pkey;
       public            postgres    false    219            �           2606    92774    orders orders_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.orders DROP CONSTRAINT orders_pkey;
       public            postgres    false    217            �           2606    92784    product product_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.product DROP CONSTRAINT product_pkey;
       public            postgres    false    218            �           2606    92762    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    215            �           2606    92795 '   order_detail order_detail_order_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.order_detail
    ADD CONSTRAINT order_detail_order_id_fkey FOREIGN KEY (order_id) REFERENCES public.orders(id);
 Q   ALTER TABLE ONLY public.order_detail DROP CONSTRAINT order_detail_order_id_fkey;
       public          postgres    false    217    3237    219            �           2606    92800 )   order_detail order_detail_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.order_detail
    ADD CONSTRAINT order_detail_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.product(id);
 S   ALTER TABLE ONLY public.order_detail DROP CONSTRAINT order_detail_product_id_fkey;
       public          postgres    false    3239    219    218            �           2606    92775    orders orders_user_id_fkey    FK CONSTRAINT     y   ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);
 D   ALTER TABLE ONLY public.orders DROP CONSTRAINT orders_user_id_fkey;
       public          postgres    false    3233    215    217            �           2606    92785     product product_merchant_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_merchant_id_fkey FOREIGN KEY (merchant_id) REFERENCES public.merchant(id);
 J   ALTER TABLE ONLY public.product DROP CONSTRAINT product_merchant_id_fkey;
       public          postgres    false    218    3235    216            >   �   x�EϹmD1���⥣�'�(a�����&rx�E��i��g�NhP��i����|�����5��y=��^������V� ����v>��k'�2g6τ���Cm����~ʇr�vٸ�d0RauhFᣱs�M�C%�H�,H��#usl]T�yp�U���j��H)�~[T      A   W  x��1�e!c�^�%�{�����%t��"�=%�E�ו3+J"�a+�H� �M0�_��d^��I�y�q	��#�z��Y�ѠϞ�-4�|�	��:����>/��w��X0��j��@h.�p�3�N,Mr����l��}���O�/�H\�Q`�n,�8y�yhFLY-?g��n2Z����=2��Wkm�ܷIKA؎&j���>�CX� +���p�2}�'���xne��_6��{1�U�X�e���n(�,9�:�� �>�[[�HTF'����Y[�Cv)��o�0��E�>�J
Ф�;Qs��I��0���v𺚙����}����<      ?     x�E�1nD1k���6��G� I�8�j;��F��*Ax� !EP����t����(��>3�����n3B�C .lo�b�Y����#:`���*��1��zٛO/>79:�M�� �;���)�e�gf��` ֻᰕ{R��k������:N�m@� 	%����J�}_�r�"~��$o�Mi�{
��i���ŗf��rs=�%��,�Õvt����7��u��K��g���9�|y�G�9J�&�&Ȏ��t�yw�{�q��>����4z�      @     x�5�1n�1���w�)�=@�Z�j�_:���և�N3LA\6p�q�O*���D���+�6�/i����1��60B=Lx�O���Xp�OY��2�;�U�{x��Ԩ�T�=6��	��6Wv��W�9����G`R�Ԁc��8��������	�V�*�	�����_|6_e�3���W;��*m�=(�M�&/�f|�NB�C�S�k��fY�tt�q�ĽL+z���+?133�͕�j]fۃ�`XV�;j�Cƈ��ʢ+���y�?�{s�      =   2  x���1N1 k�/F�c;I�Ch�ĩ@ N�OtKy�6�(�F�ئw�ae�K@�pXLXQ��ZO߷���/���o�<>ޟ>�v����tH6�n���� �0��4�W�w�|��Û�X= ;�����x��:{���CM9:�ޤ!dK�%��
UF�=[�] 'j�p�%�"���&	Tɋs	�z��F���uAT�ҝ�V@��iV1��8�`h�g@�b�R�m�K3�$��T���|MP$Tך<��Jb�=�������Oe
'�Y^H\�4R
���H��*M��Y��_����+؜     