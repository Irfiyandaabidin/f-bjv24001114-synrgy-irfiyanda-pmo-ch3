package com.example.challege3;

import com.example.challege3.entity.Food;
import com.example.challege3.entity.FoodOrder;
import com.example.challege3.service.CartService;
import com.example.challege3.service.impl.ImplCartService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.function.BiConsumer;
import java.util.function.Supplier;


@SpringBootApplication
public class Challege3Application {
	static Scanner input = new Scanner(System.in);
	static CartService cartService = new ImplCartService();

	static Supplier<String> notValidInput = () -> "Input yang anda masukkan salah";

	static void menu() {
		List<Food> listMenu = new ArrayList<>();
		listMenu.add(new Food(1, "Nasi Goreng", 15000));
		listMenu.add(new Food(2, "Mie Goreng", 13000));
		listMenu.add(new Food(3, "Nasi + Ayam", 18000));
		listMenu.add(new Food(4, "Es Teh Manis", 3000));
		listMenu.add(new Food(5, "Es Jeruk", 5000));

		boolean ulang = true;
		do {
			System.out.println("================================");
			System.out.println("Selamat datang di BinarFud");
			System.out.println("================================");
			for(int i = 1; i < listMenu.size(); i++) {
				int index = i;
				// Penerapan Lambda untuk tampilkan menu dengan biconsumer (2 input)
				BiConsumer<String, Integer> printMenu = (food, price) -> System.out.println(index + ". " + food + "\t\t|" + price);
				Food food = listMenu.get(i-1);
				printMenu.accept(food.getNama(), food.getHarga());
			}
			System.out.println("99. Pesan dan Bayar");
			System.out.println("0. Keluar Aplikasi");
			System.out.print("=> ");
			try {
				int selectMenu = input.nextInt();
				if(selectMenu == 0) {
					ulang = false;
				}
				if(selectMenu == 99) {
					konfirmasiPembayaran();
				}
				Food selectedFood = listMenu.get(selectMenu-1);
				FoodOrder foodOrder = new FoodOrder(selectedFood.getCode(), selectedFood.getNama(), selectedFood.getHarga());
				tambahPesanan(foodOrder);
			} catch (Exception exception) {
				System.out.println(notValidInput.get());
				input.next();
			}
		} while (ulang);
	}

	static void tambahPesanan(FoodOrder foodOrder) {
		System.out.println("================================");
		System.out.println("Berapa pesanan anda");
		System.out.println("================================");
		System.out.println("\n" + foodOrder.getNama() + "\t | " + foodOrder.getHarga());
		System.out.println("(input 0 untuk hapus pesanan)");
		System.out.println("(input 99 untuk kembali)");
		System.out.print("\nqty => ");

		try {
			int qty = input.nextInt();
			if (qty == 99) {
				menu();
			} else if (qty == 0) {
				// Hapus pesanan
				foodOrder.setQty(qty);
				cartService.addCart(foodOrder);
				System.out.println("Hapus pesanan");
			} else {
				foodOrder.setQty(qty);
				cartService.addCart(foodOrder);
			}
		} catch (Exception e) {
			System.out.println(notValidInput.get());
			input.next();
		}
	}

	static void konfirmasiPembayaran() {
		System.out.println("================================");
		System.out.println("Konfirmasi & Pembayaran");
		System.out.println("================================\n");
		System.out.println(cartService.getCart().getListOrder().values());
		int totalHargaItems = cartService.getCart().getListOrder().values().stream()
				.mapToInt(item -> {
					int totalHargaItem = item.getHarga() * item.getQty();
					System.out.println(item.getNama() + "\t\t" + item.getQty() + "\t" + item.getHarga());
					return totalHargaItem;
				})
				.sum();
		int totalItem = cartService.getCart().getListOrder().values().stream()
				.mapToInt(FoodOrder::getQty)
				.sum();

		System.out.println("-------------------------------------+");
		System.out.println("Total \t\t\t" + totalItem + "\t" + totalHargaItems);
		System.out.println("\n1. Konfirmasi dan Bayar");
		System.out.println("2. Kembali ke menu utama");
		System.out.println("0. Keluar aplikasi");
		System.out.print("\n=> ");
		int selectMenu = input.nextInt();
		switch (selectMenu) {
			case 1:
				try {
					FileWriter strukWriter = new FileWriter("struk.txt");
					strukWriter.write("=======================\n");
					strukWriter.write("BinarFud\n");
					strukWriter.write("=======================\n\n");
					strukWriter.write("Terima kasih sudah memesan di BinarFud\n");
					strukWriter.write("Dibawah ini adalah pesanan anda\n\n");
					for (FoodOrder item : cartService.getCart().getListOrder().values()) {
						strukWriter.write(item.getNama() + "\t\t" + item.getQty() + "\t" + item.getHarga() + "\n");
					}
					strukWriter.write("\n-------------------------------------+ \n\n");
					strukWriter.write("Total \t\t\t" + totalItem + "\t" + totalHargaItems +"\n");
					strukWriter.write("Pembayaran : BinarCash\n\n");
					strukWriter.write("=======================\n");
					strukWriter.write("Simpan struk ini sebagai bukti pembayaran\n");
					strukWriter.write("=======================\n");
					strukWriter.close();
					System.exit(0);
				} catch (IOException e) {
					System.out.println("An error occured");
					e.printStackTrace();
				}
				break;
			case 2:
				menu();
			case 0:
				System.exit(0);
			default:
				menu();
		}
	}

	public static void main(String[] args) {
		menu();
	}
}
