package com.example.challege3.entity;

import lombok.Data;

@Data
public class Food {
    private int code;
    private String nama;
    private int harga;

    public Food(int code, String nama, int harga) {
        this.code = code;
        this.nama = nama;
        this.harga = harga;
    }

    public Food() {
    }
}