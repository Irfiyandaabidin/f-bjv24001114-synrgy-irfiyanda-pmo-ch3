package com.example.challege3.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class Cart {
    private Map<Integer, FoodOrder> ListOrder = new HashMap();
}