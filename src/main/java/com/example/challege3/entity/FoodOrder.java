package com.example.challege3.entity;

import lombok.Data;

@Data
public class FoodOrder extends Food {
    private int qty;

    public FoodOrder() {
        super();
        this.qty = 1;
    }

    public FoodOrder(int code, String nama, int harga) {
        super(code, nama, harga);
        this.qty = 1;
    }

    public FoodOrder(int code, String nama, int harga, int qty) {
        super(code, nama, harga);
        this.qty = qty;
    }
}