package com.example.challege3.service;

import com.example.challege3.entity.Cart;
import com.example.challege3.entity.FoodOrder;

public interface CartService {
    public void addCart(FoodOrder food);

    public Cart getCart();
}