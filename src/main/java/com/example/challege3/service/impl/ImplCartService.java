package com.example.challege3.service.impl;

import com.example.challege3.entity.Cart;
import com.example.challege3.entity.FoodOrder;
import com.example.challege3.service.CartService;

public class ImplCartService implements CartService {
    private Cart cart = new Cart();

    @Override
    public void addCart(FoodOrder food) {
        try {
            if(food.getQty() < 0) {
                throw new IllegalArgumentException("Quantity tidak bisa negative");
            }
            if(food.getQty() == 0) {
                cart.getListOrder().remove(food.getCode());
                System.out.println("Berhasil menghapus " + food.getNama() + " dari daftar pesanan");
                return;
            }
            if(cart.getListOrder().containsKey(food.getCode())) {
                FoodOrder foodSelect = cart.getListOrder().get(food.getCode());
                foodSelect.setQty(foodSelect.getQty() + food.getQty());
                System.out.println("Berhasil menambah qty " + food.getNama() + " dari daftar pesanan");
                return;
            }
            cart.getListOrder().put(food.getCode(), food);
            System.out.println("Berhasil menambahkan " + food.getNama() + " ke pesanan");
        } catch (IllegalArgumentException exception) {
            System.out.println("Gagal menambahkan " + food.getNama() + " ke pesanan: " + exception.getMessage());
        }
    }

    @Override
    public Cart getCart() {
        return cart;
    }
}