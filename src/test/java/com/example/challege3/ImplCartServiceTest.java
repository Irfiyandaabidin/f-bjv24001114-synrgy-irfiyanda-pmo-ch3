package com.example.challege3;

import com.example.challege3.entity.FoodOrder;
import com.example.challege3.service.CartService;
import com.example.challege3.service.impl.ImplCartService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ImplCartServiceTest {
    private CartService cartService;

    @BeforeEach
    void setUp() {
        cartService = new ImplCartService();
    }

    @Test
    void testAddCart() {
        FoodOrder foodOrder1 = new FoodOrder(1, "Nasi Goreng", 15000);
        foodOrder1.setQty(2);
        cartService.addCart(foodOrder1);
        Assertions.assertEquals(1, cartService.getCart().getListOrder().size());
        Assertions.assertEquals(2, cartService.getCart().getListOrder().get(1).getQty());

        FoodOrder foodOrder2 = new FoodOrder(2, "Mie Goreng", 13000);
        foodOrder2.setQty(3);
        cartService.addCart(foodOrder2);
        Assertions.assertEquals(2, cartService.getCart().getListOrder().size());
        Assertions.assertEquals(3, cartService.getCart().getListOrder().get(2).getQty());
    }

    @Test
    void testAddNegativeQuantity() {
        FoodOrder foodOrder = new FoodOrder(3, "Nasi + Ayam", 18000);
        foodOrder.setQty(-2);
        cartService.addCart(foodOrder);
        Assertions.assertEquals(0, cartService.getCart().getListOrder().size());
    }

    @Test
    void testAddZeroQuantity() {
        FoodOrder foodOrder = new FoodOrder(4, "Es Teh Manis", 3000);
        foodOrder.setQty(0);
        cartService.addCart(foodOrder);
        Assertions.assertEquals(0, cartService.getCart().getListOrder().size());
    }

    @Test
    void testAddExistingItem() {
        FoodOrder foodOrder1 = new FoodOrder(5, "Es Jeruk", 5000);
        foodOrder1.setQty(2);
        FoodOrder foodOrder2 = new FoodOrder(5, "Es Jeruk", 5000);
        foodOrder2.setQty(3);
        cartService.addCart(foodOrder1);
        cartService.addCart(foodOrder2);
        Assertions.assertEquals(1, cartService.getCart().getListOrder().size());
        Assertions.assertEquals(5, cartService.getCart().getListOrder().get(5).getQty());
    }
}
