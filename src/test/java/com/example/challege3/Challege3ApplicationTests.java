package com.example.challege3;

import com.example.challege3.service.CartService;
import com.example.challege3.service.impl.ImplCartService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Challege3ApplicationTests {
	private CartService cartService;

	@Test
	void contextLoads() {
	}

	@BeforeEach
	public void setUp() {
		cartService = new ImplCartService();
	}

}
